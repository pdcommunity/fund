import React from "react";
import { Navbar } from "./Navbar";
import { Container } from "react-bootstrap";

export const DefaultTemplate = ({ children, dark }) => {
  const c = dark ? "bg-dark text-white" : '';
  return <div className={c} style={{ minHeight: '100vh' }}>
    <Navbar/>
    <div style={{ height: '70px' }}></div>
    <Container>
      {children}
    </Container>
  </div>;
};
