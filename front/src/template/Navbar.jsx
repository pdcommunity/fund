import React from "react";
import { Nav, Navbar as BootstrapNavbar } from "react-bootstrap";
import { Link } from "react-router-dom";
import { UserNavbar } from "../login/UserNavbar";
import { debugReset } from "../util/debug";

export const Navbar = () => (
  <BootstrapNavbar bg="primary" variant="dark">
    <Nav>
      <Nav.Link as={Link} to="/">خانه</Nav.Link>
      <Nav.Link as={Link} to="/project">حمایت</Nav.Link>
      <Nav.Link onClick={debugReset}>بازنشانی به حالت اولیه</Nav.Link>
    </Nav>
    <UserNavbar/>
  </BootstrapNavbar>
);
