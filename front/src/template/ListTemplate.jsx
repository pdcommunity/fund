import React from "react";
import { Button } from "react-bootstrap";
import { DefaultTemplate } from "./DefaultTemplate";
import { Link } from "react-router-dom";

export const ListTemplate = ({ children }) => {
  return <DefaultTemplate>
    <div style={{
      textAlign: 'center',
      margin: '-40px 0 20px 0',
    }}>
      <Button as={Link} to="/project">کارزار</Button>&emsp;
      <Button as={Link} to="/infra">زیر ساخت</Button>&emsp;
      <Button as={Link} to="/people">افراد</Button>
    </div>
    {children}
  </DefaultTemplate>;
};
