/* eslint-disable no-unused-vars */
/* eslint-disable prefer-arrow/prefer-arrow-functions */
import React from "react";
import {
  BrowserRouter as Router,
  Switch,
  Route,
} from "react-router-dom";
import { ProjectRouter } from "./projects/ProjectRouter";
import { DefaultTemplate } from "./template/DefaultTemplate";
import { Home } from "./home/Home";
import { UserWrapper } from "./login/UserContext";
import { NotFound } from "./template/NotFound";

export default function App() {
  return (
    <Router>
      <UserWrapper>
        <Switch>
          <Route path="/project">
            <ProjectRouter/>
          </Route>
          <Route path="/users">
            <Users/>
          </Route>
          <Route path="/" exact>
            <Home/>
          </Route>
          <Route path="*">
            <NotFound/>
          </Route>
        </Switch>
      </UserWrapper>
    </Router>
  );
}

function Users() {
  return <DefaultTemplate><h2>Users</h2></DefaultTemplate>;
}
