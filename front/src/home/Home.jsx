import React from "react";
import { Navbar } from "../template/Navbar";
import { Link } from "react-router-dom";
import { Button } from "react-bootstrap";

export const Home = () => {
  return <div>
    <Navbar/>
    <div style={{ width: '100%', height: '100vh', backgroundColor: 'black' }}></div>
    <div style={{
      position: 'absolute',
      top: '50vh',
      left: '50vw',
      transform: 'translate(-50%,0)',
      textAlign: 'center',
    }}>
      <h1 className="text-primary" style={{ fontSize: '10vh', whiteSpace: 'nowrap' }}>
        تامین مالی
        <br/>
        داده های عمومی
      </h1>
      <Button size="lg" as="a" variant="secondary" href="https://pdcommunity.ir/">داده عمومی چیست؟</Button>
      &emsp;
      <Button size="lg" as={Link} to="/project">مشاهده کارزار ها</Button>
    </div>
  </div>;
};
