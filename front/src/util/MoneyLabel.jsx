import React from "react";

export const MoneyLabel = ({ money }) => {
  const t = ("" + money).replace(/\B(?=(\d{3})+(?!\d))/g, ",");
  return <span> {t} تومان </span>;
};
