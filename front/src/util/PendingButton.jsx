import { Button, Spinner } from "react-bootstrap";
import React from "react";

export const PendingButton = ({ pending, children, ...props }) => {
  return <Button {...props} >
    {pending ? <Spinner animation="border" as="span"/> : children}
  </Button>;
};
