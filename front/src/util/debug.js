import { getFromApi } from "./api";

export const debugReset = async () => {
  localStorage.clear();
  await getFromApi('debug/init');
  window.location.reload();
};
