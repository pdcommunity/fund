import { useEffect, useState } from "react";

export const getFromApi = async (url, body = {}) => {
  try {
    const res = await fetch(`/api/${url}`, {
      headers: {
        'Content-type': 'application/json',
      },
      method: 'POST',
      body: JSON.stringify(body),
    });
    return await res.json();
  } catch (e) {
    return {
      ok: false,
      reason: 'client side error',
      error: e,
    };
  }
};

export const useApi = (...params) => {
  const [result, setResult] = useState({ loading: true, params: {} });
  useEffect(()=>{
    (async ()=>{
      console.log(params, result);
      if (JSON.stringify(params) === JSON.stringify(result.params)) {
        return;
      }
      setResult({
        params,
        value: await getFromApi(...params),
        loading: false,
      });
    })();
  });
  return result;
};
