import React, { useState } from "react";

export const MoneyInput = ({ value = 0, onChange = ()=>{} }) => {
  const [m, setM] = useState(value);
  const numToPretty = (n) => {
    return ("" + n).replace(/\B(?=(\d{3})+(?!\d))/g, ",");
  };
  const prettyToNum = (t) => {
    return 1 * t.replace(/,/g, '');
  };
  const myChange = (e) => {
    const v = prettyToNum(e.target.value);
    setM(v);
    onChange(v);
  };
  return <div className="display-4" style={{
    backgroundColor: 'white',
    border: 'solid #f44333',
    padding: '0.7em',
    borderRadius: '0.5em',
    display: 'inline-block',
  }}>
    <input style={{
      outline: 'none',
      border: 'none',
      textAlign: 'center',
      color: '#f44333',
      width: '15ch',
    }} value={numToPretty(m)} onChange={myChange}></input>
    <span className="text-primary">تومان</span>
  </div>;
};
