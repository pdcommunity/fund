import React from 'react';
import { CardColumns } from 'react-bootstrap';
import { ProjectItem } from './ProjectItem';
import { DefaultTemplate } from '../template/DefaultTemplate';
import { useApi } from '../util/api';
import { ListTemplate } from '../template/ListTemplate';

export const ProjectList = () => {
  const { loading, value: result } = useApi('project/list');
  if (loading) {
    return <DefaultTemplate>در حال بارگیری</DefaultTemplate>;
  }
  const value = result.value;
  return <ListTemplate>
    <CardColumns>
      {value.map((project) => <ProjectItem key={project.id} project={project}/>)}
    </CardColumns>
  </ListTemplate>;
};
