import React from 'react';
import {
  Button, Card, ProgressBar,
} from 'react-bootstrap';
import { Link } from "react-router-dom";
import { MoneyLabel } from '../util/MoneyLabel';

/**
 *
 * @param {Object} props
 * @param {import('../../../types/project').Project} props.project
 */
export const ProjectItem = (props) => {
  const {
    imageSource: src = '/dist/default.png', name,
    description = '', id, moneyNeed, moneyHave,
  } = props.project;
  return <Card style={{ width: '18rem' }}>
  <Card.Img variant="top" src={src}/>
  <Card.Body>
    <Card.Title>{name}</Card.Title>
    <Card.Text>
      مبلغ مورد نیاز:
      <MoneyLabel money={moneyNeed - moneyHave}/>
      <ProgressBar now={(moneyHave * 100) / moneyNeed}></ProgressBar>
      {description}
    </Card.Text>
    <Button as={Link} variant="primary" to={`/project/${id}`} block>مشاهده و حمایت</Button>
  </Card.Body>
</Card>;
};
