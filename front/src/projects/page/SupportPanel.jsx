import React, { useContext } from "react";
import {
  Card, Button, ListGroup,
} from "react-bootstrap";
import { Link, useHistory } from "react-router-dom";
import { UserContext } from "../../login/UserContext";

const MyButton = ({ user, ...props }) => {
  if (!user.loggedIn) {
    return <Button block {...props} onClick={user.login}/>;
  }
  return <Button block {...props}/>;
};

export const SupportPanel = ({ project }) => {
  const user = useContext(UserContext);
  const history = useHistory();
  return <Card>
    <Card.Header>
      حمایت کنید
    </Card.Header>
    <ListGroup variant="flush">
      <ListGroup.Item>
      <Button block>حمایت ناشناس</Button>
      توجه کنید که در صورت شکست خوردن پویش و عدم تامین مبلغ مورد نیاز
      ما قادر به بازگشت پول به حساب شما نیستیم.
      </ListGroup.Item>
      <ListGroup.Item>
        {!user.loggedIn && 'یا وارد شوید و از گزینه های زیر استفاده کنید'}
      <MyButton user={user} onClick={()=>{
        history.push(`${project.id}/pay`, { project });
      }}>حمایت دلخواه</MyButton>
      <MyButton user={user}>طرح ۱</MyButton>
      </ListGroup.Item>
    </ListGroup>
  </Card>;
};
