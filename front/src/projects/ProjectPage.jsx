import React from "react";
import { useParams } from "react-router-dom";
import { DefaultTemplate } from "../template/DefaultTemplate";
import { useApi } from "../util/api";
import {
  Col, Row, Card, Image, ProgressBar,
} from "react-bootstrap";
import { MoneyLabel } from "../util/MoneyLabel";
import { SupportPanel } from "./page/SupportPanel";
import { ClientError } from "../template/ClientError";
import { NotFound } from "../template/NotFound";

export const ProjectPage = () => {
  const { id } = useParams();
  /**
   * @type {{ value: import("../../../types/project").Project}}
   */
  const { loading, value: project = {} } = useApi(`project/get/${id}`);
  if (loading) {
    return <DefaultTemplate>در حال بارگیری</DefaultTemplate>;
  }
  if (!project.ok) {
    if (project.reason === 'not found') {
      return <NotFound/>;
    }
    return <ClientError/>;
  }
  const {
    imageSource: src = '/dist/default.png', name,
    description = '', moneyNeed, moneyHave,
  } = project;
  return <DefaultTemplate>
    <Row>
    <Col xs="8">
      <h1>{name}</h1>
      <Image src={src} fluid/>
      <p>{description}</p>
    </Col>
    <Col>
      <Card>
        <Card.Body>
          <Card.Text>
            تاکنون مبلغ
            <MoneyLabel money={moneyHave}/>
            از
            <MoneyLabel money={moneyNeed}/>
            جمع شده است
          </Card.Text>
          <ProgressBar now={(moneyHave * 100) / moneyNeed}></ProgressBar>
        </Card.Body>
      </Card>
      <div style={{ height: '20px' }}/>
      <SupportPanel project={project}/>
    </Col>
    </Row>
  </DefaultTemplate>;
};
