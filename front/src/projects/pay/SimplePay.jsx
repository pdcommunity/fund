import React, { useState, useEffect, useContext } from "react";
import { DefaultTemplate } from "../../template/DefaultTemplate";
import { Button, Alert } from "react-bootstrap";
import { MoneyInput } from "../../util/MoneyInput";
import { useHistory } from "react-router-dom";
import { ClientError } from "../../template/ClientError";
import { getFromApi } from "../../util/api";
import { PendingButton } from "../../util/PendingButton";
import { UserContext } from "../../login/UserContext";

const BuyButton = ({ loggedIn, pending, onClick }) => {
  if (loggedIn) {
    return <PendingButton pending={pending} size="lg" onClick={onClick}>
      پرداخت
    </PendingButton>;
  }
  return <PendingButton pending={pending} size="lg" disabled={true}>
    ابتدا وارد شوید
  </PendingButton>;
};

export const SimplePay = () => {
  const [price, setPrice] = useState(1000);
  const [pending, setPending] = useState(false);
  const [et, setEt] = useState('');
  const history = useHistory();
  const user = useContext(UserContext);
  const s = history.location.state;
  useEffect(()=>{
    if (s === undefined) {
      history.replace(history.location.pathname.split('/').slice(0, -1).join('/'));
    }
  });
  if (s === undefined) {
    return <ClientError/>;
  }
  const { project } = s;
  const pay = async () => {
    setPending(true);
    const r = await getFromApi('project/pay', {
      plan: 'simple',
      projectId: project.id,
      token: user.token,
      price,
    });
    if (r.ok) {
      history.push(`/project/${project.id}`);
    } else {
      if (r.reason === 'auth failed') {
        user.logout();
      }
      setEt(r.reason);
      setPending(false);
    }
  };
  const ebox = (() => {
    if (et === 'not enough money') {
      return <Alert
        variant="danger"
        onClose={() => setEt('')}
        dismissible
      >موجودی کافی نیست</Alert>;
    }
    return <div/>;
  })();
  return <DefaultTemplate dark>
    <div style={{ textAlign: 'center' }}>
      <h1>پرداخت</h1>
      <MoneyInput value={price} onChange={setPrice}/>
      {ebox}
      <div style={{ padding: '1em' }}>
        <BuyButton pending={pending} onClick={pay} loggedIn={user.loggedIn}/>&emsp;
        <Button size="lg" variant="secondary" onClick={()=>history.go(-1)}>بازگشت</Button>
      </div>
      <p>
        شما این مبلغ را به
        {` ${project.id} `}
        بابت
        {` ${project.name} `}
        پرداخت می کنید. جمعیت داده های عمومی
        هیچ مسئولیتی در قبال این پول و کاری که با آن انجام می شود ندارد.
      </p>
    </div>
  </DefaultTemplate>;
};
