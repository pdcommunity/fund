/* eslint-disable no-unused-vars */
/* eslint-disable prefer-arrow/prefer-arrow-functions */
import React from "react";
import {
  BrowserRouter as Router,
  Switch,
  Route,
  Link,
  useRouteMatch,
} from "react-router-dom";
import { ProjectList } from "./ProjectList";
import { ProjectPage } from "./ProjectPage";
import { SimplePay } from "./pay/SimplePay";

export const ProjectRouter = () => {
  const match = useRouteMatch();
  return (
    <Switch>
      <Route path={`${match.path}/:id/pay`}>
        <SimplePay/>
      </Route>
      <Route path={`${match.path}/:id`}>
        <ProjectPage/>
      </Route>
      <Route path={match.path}>
        <ProjectList/>
      </Route>
    </Switch>
  );
};
