import React, { useState } from "react";
import {
  Modal, Button, Form, Spinner,
} from "react-bootstrap";
import { getFromApi } from "../util/api";


export const LoginModal = (props) => {
  const [show, setShow] = props.showState;
  const [pending, setPending] = useState(false);
  const [username, setUsername] = useState('');
  const [password, setPassword] = useState('');
  const loginMode = show.slice(-5) === 'login';
  const commandText = (loginMode ? "ورود" : 'بساز');
  const swich = () => {
    if (loginMode) {
      setShow('createAccount');
    } else {
      setShow('login');
    }
  };
  return <Modal show={show.slice(0, 4) !== 'none'} onHide={()=>{
    if (!pending) setShow('none-' + show);
  }}>
    <Modal.Header closeButton>
      <Modal.Title>وارد شوید</Modal.Title>
    </Modal.Header>

    <Modal.Body>
      <Form>
        <Form.Group controlId="formBasicEmail">
          <Form.Label>نام کاربری</Form.Label>
          <Form.Control
            value={username} dir="ltr" type="text"
            placeholder="نام کاربری خود را وارد کنید"
            onChange={(e) => setUsername(e.target.value)}
          />
        </Form.Group>
        <Form.Group controlId="formBasicPassword">
          <Form.Label>رمز عبور</Form.Label>
          <Form.Control dir="ltr" type="password" placeholder="رمز عبور" value={password}
            onChange={(e) => setPassword(e.target.value)}
          />
        </Form.Group>
      </Form>
    </Modal.Body>

    <Modal.Footer>
      <Button variant="secondary" onClick={swich}>{
        loginMode ? 'من حساب ندارم' : 'من حساب دارم'}</Button>
      &emsp;
      <Button variant="primary" onClick={async ()=>{
        setPending(true);
        const url = loginMode ? 'user/login' : 'user/createAccount';
        const result = await getFromApi(url, {
          username, password,
        });
        setPending(false);
        if (result.ok) {
          if (loginMode) {
            props.setUser({ loggedIn: true, userObj: result.userObj, token: result.token });
            setShow('none-' + show);
          } else {
            setShow('login');
          }
        }
      }}>{
        pending ? <Spinner animation="border" as="span"/> : commandText
      }</Button>
    </Modal.Footer>
  </Modal>;
};
