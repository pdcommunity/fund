import React, { useState, useEffect } from "react";
import { LoginModal } from "./LoginModal";
import { subscribe } from "../websocket/subscribe";

export const UserContext = React.createContext();

const defaultUser = () => {
  const x = window.localStorage.getItem('user');
  if (!x) return { loggedIn: false };
  const user = JSON.parse(x);
  return user;
};

export const UserWrapper = (props) => {
  const loginState = useState('none-login');
  const [user, setUserP] = useState(defaultUser());
  const username = user.loggedIn ? user.userObj.username : '#';
  const login = () => {
    loginState[1]('login');
  };
  const createAccount = () => {
    loginState[1]('createAccount');
  };
  const setUser = (d) => {
    setUserP(d);
    window.localStorage.setItem('user', JSON.stringify(d));
  };
  useEffect(() => {
    if (username !== '#') {
      return subscribe(`money:${username}`, (x) => {
        const userObj = {
          ...user.userObj,
          money: x,
        };
        setUser({
          ...user,
          userObj,
        });
      });
    }
    return () => {};
  }, [username]);
  const logout = () => setUser({ loggedIm: false });
  return <UserContext.Provider value={{
    ...user, login, logout, createAccount,
  }}>
    {props.children}
    <LoginModal showState={loginState} setUser={setUser}/>
  </UserContext.Provider>;
};
