import React, { useContext } from "react";
import { Nav } from "react-bootstrap";
import { UserContext } from "./UserContext";
import { MoneyLabel } from "../util/MoneyLabel";
import { Link } from "react-router-dom";

export const UserNavbar = () => {
  const user = useContext(UserContext);
  if (user.loggedIn) {
    return <Nav className="mr-auto">
      <Nav.Link as={Link} to="/user">
        خوش آمدید {user.username}
      </Nav.Link>
      <Nav.Link>
        موجودی شما:
        <MoneyLabel money={user.userObj.money}/>
      </Nav.Link>
      <Nav.Link onClick={user.logout}>
        خروج
      </Nav.Link>
    </Nav>;
  }
  return <Nav className="mr-auto">
    <Nav.Link onClick={user.login}>وارد شوید</Nav.Link>
  </Nav>;
};
