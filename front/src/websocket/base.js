/* eslint-disable immutable/no-mutation */
/* eslint-disable toplevel/no-toplevel-let */

/**
 * @type {WebSocket}
 */
let ws;
const initMessages = [];
let opened = false;
let receiver;

export const sendText = (x) => {
  if (opened) {
    ws.send(x);
  } else {
    initMessages.push(x);
  }
};

export const send = (x) => sendText(JSON.stringify(x));

export const init = () => {
  try {
    ws = new WebSocket(`ws://${window.location.host}/websocket/`);
    ws.onopen = (() => {
      opened = true;
      initMessages.forEach(x => ws.send(x));
    });
    ws.onmessage = (msg) => {
      if (receiver) receiver(msg);
    };
    ws.onclose = () => {
      // alert('pokidim');
    };
  } catch (e) {
    console.log(e);
  }
};

export const registerReceiver = (x) => {
  receiver = x;
};
