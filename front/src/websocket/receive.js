import { registerReceiver } from "./base";
import { onReceive } from "./subscribe";

export const startReceiving = () => {
  registerReceiver((msg) => {
    const x = JSON.parse(msg.data);
    console.log(x);
    if (x.type === 'subscribe-event') {
      onReceive(x);
    }
  });
};
