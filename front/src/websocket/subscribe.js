import { send } from "./base";

const table = {};
const value = {};
// eslint-disable-next-line toplevel/no-toplevel-let
let cnt = 0;

const register = (name, callback) => {
  if (table[name] === undefined) {
    // eslint-disable-next-line immutable/no-mutation
    table[name] = [];
  }
  cnt += 1;
  table[name].push({
    key: cnt,
    callback,
  });
  return cnt;
};

const clear = (name, id) => {
  // eslint-disable-next-line immutable/no-mutation
  table[name] = table[name].filter((x) => x.key !== id);
};

export const subscribe = (name, callback) => {
  const id = register(name, callback);
  if (table[name].length === 1) {
    send({
      type: 'subscribe',
      name,
    });
  } else if (value[name] !== undefined) {
    callback(value[name]);
  }
  return ()=>{
    clear(name, id);
    if (table[name].length === 0) {
      send({
        type: 'unsubscribe',
        name,
      });
    }
  };
};

export const onReceive = ({ name, value: data }) => {
  // eslint-disable-next-line immutable/no-mutation
  value[name] = data;
  table[name].forEach(x => {
    x.callback(data);
  });
};
