export interface Project {
  id: String,
  imageSource: String,
  name: String,
  description: String,
  moneyNeed: Number,
  moneyHave: Number,
};