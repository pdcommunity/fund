/* eslint-disable immutable/no-mutation */
import Koa from "koa";
import koaSend from "koa-send";
import { projectRoot } from "../util/rootAddress.mjs";
import { apiHandler } from "../api/index.mjs";
import bodyparser from "koa-bodyparser";
import ws from "koa-websocket";
import { websocketHandler } from "../api/websocket/index.mjs";

const frontHandler = async (ctx) => {
  if (ctx.path.substr(0, 6) === '/dist/') {
    await koaSend(ctx, 'front/dist/' + ctx.path.substr(6), { root: projectRoot });
  } else {
    await koaSend(ctx, 'front/index.html', { root: projectRoot });
  }
};

export const serverBuilder = async ({ port }) => {
  const server = ws(new Koa());
  server.use(async (ctx, next) => {
    try {
      await next();
    } catch (err) {
      err.status = err.statusCode || err.status || 500;
      ctx.body = err.toString() + "\n";
    }
  });
  server.use(bodyparser());
  server.use(await apiHandler({
    redisParams: {
      host: '127.0.0.1',
      port: '6379',
    },
  }));
  server.use(frontHandler);
  server.ws.use(websocketHandler);
  server.listen(port);
};
