import fs from "fs";
import { promisify } from "util";
import path from "path";
import { projectRoot } from "../util/rootAddress.mjs";

const writeFile = promisify(fs.writeFile);

const main = async () => {
  const nginxFile = path.join(projectRoot, 'nginx', 'nginx.conf');
  await writeFile(nginxFile, `
# include ${nginxFile}
server {
  charset UTF-8;
  listen 80;
  server_name func.vcap.me fund.pdcommunity.ir;
  root ${path.join(projectRoot, 'front')}/;
  location / {
    try_files '' /index.html = 404;
  }
  location /dist {
  }
  location /api {
    proxy_pass http://127.0.0.1:15985;
  }
  location /websocket/ {
    proxy_pass http://127.0.0.1:15985;
    proxy_http_version 1.1;
    proxy_set_header Upgrade $http_upgrade;
    proxy_set_header Connection "upgrade";
  }
}
  `);
};

// eslint-disable-next-line toplevel/no-toplevel-side-effect
main();
