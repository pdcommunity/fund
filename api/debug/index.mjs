/* eslint-disable immutable/no-mutation */

import { router } from "../util/router.mjs";
import { createAccount } from "../user/auth.mjs";
import { createProject } from "../project/index.mjs";

const initHandler = async ({ ctx, rc }) => {
  try {
    await rc.flushall();
    await createAccount(rc, { username: 'h', password: '2' });
    await rc.incrby('money:h', 200000);
    await createProject(rc, {
      id: 'project1',
      name: 'نوشتن کتاب فلان',
      imageSource: 'https://pdcommunity.ir/images/main-background.svg',
      moneyNeed: 2300000,
    });
    await createProject(rc, {
      id: 'project2',
      name: 'تولید نرم افزار بهمان',
      description: 'نرم افزار بهمان یک نرم افزار است که به شما کمک می کند تا بهمان های خود را مدیریت کنید.',
      moneyNeed: 500000000,
    });
    ctx.body = { ok: true };
  } catch (e) {
    ctx.body = { ok: false, reason: e + "" };
  }
};

export const debugHandler = (params) => {
  return router(params, [
    { key: '/init', callback: initHandler },
  ]);
};
