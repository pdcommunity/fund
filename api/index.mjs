/* eslint-disable immutable/no-mutation */
import Redis from "async-redis";
import { userHanler } from "./user/index.mjs";
import { router } from "./util/router.mjs";
import { projectHandler } from "./project/index.mjs";
import { debugHandler } from "./debug/index.mjs";

export const apiHandler = async ({
  redisParams: { host, port },
}) => {
  const rc = Redis.createClient(port, host);
  return (async (ctx, next) => {
    if (ctx.url.slice(0, 4) !== '/api') {
      await next();
      return;
    }
    ctx.state.parsedUrl = ctx.url.slice(4);
    await router({ ctx, rc }, [
      { key: '/user', callback: userHanler },
      { key: '/project', callback: projectHandler },
      { key: '/debug', callback: debugHandler },
    ]);
  });
};
