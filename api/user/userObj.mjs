export const getUserObj = async (username, rc) => {
  const money = await rc.get(`money:${username}`);
  return { money, username };
};
