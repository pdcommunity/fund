import { router } from "../util/router.mjs";
import { createAccountHandler, loginHandler } from "./auth.mjs";

export const userHanler = async (params) => {
  return router(params, [
    { key: '/login', callback: loginHandler },
    { key: '/createAccount', callback: createAccountHandler },
  ]);
};
