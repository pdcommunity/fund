import crypto from "crypto";
import { getUserObj } from "./userObj.mjs";

/* eslint-disable immutable/no-mutation */
export const loginHandler = async ({ ctx, rc }) => {
  const { username, password } = ctx.request.body;
  const [dbPassword, dbSalt] = await rc.hmget(`user:${username}`, 'password', 'salt');
  if (dbPassword === null) {
    ctx.body = { ok: false };
    return;
  }
  const hash = crypto.createHash('sha256').update(password + dbSalt).digest('base64');
  if (hash !== dbPassword) {
    ctx.body = { ok: false };
    return;
  }
  const userObj = await getUserObj(username, rc);
  const token = crypto.randomBytes(36).toString('base64');
  rc.set(`token:${token}`, username, 'EX', 100000);
  ctx.body = { ok: true, userObj, token };
};

export const createAccount = async (rc, { username, password }) => {
  const salt = crypto.randomBytes(36).toString('base64');
  const hash = crypto.createHash('sha256').update(password + salt).digest('base64');
  await rc.hset(`user:${username}`, 'password', hash, 'salt', salt);
  await rc.set(`money:${username}`, 100000);
};

export const createAccountHandler = async ({ ctx, rc }) => {
  const { username, password } = ctx.request.body;
  if (!await rc.sadd('names', username)) {
    ctx.body = { ok: false, reason: 'duplicate username' };
    return;
  }
  createAccount(rc, { username, password });
  ctx.body = { ok: true };
};
