import { lock, unlock } from "../util/lock.mjs";
import { emit } from "../websocket/index.mjs";

/**
 *
 * @param {import("..").RedisClient} rc
 * @param from
 * @param to
 * @param {Number} value
 */
export const transferMoney = async (rc, from, to, value) => {
  const mf = `money:${from}`;
  const tf = `money:${to}`;
  await lock(rc, mf);
  await lock(rc, tf);
  const cur = await rc.get(mf);
  if (cur - value < 0) {
    await unlock(rc, mf);
    await unlock(rc, tf);
    throw new Error('not enough money');
  }
  await rc.incrby(mf, -value);
  await rc.incrby(tf, value);
  await unlock(rc, mf);
  await unlock(rc, tf);
  emit(mf, await rc.get(mf));
  emit(tf, await rc.get(tf));
};
