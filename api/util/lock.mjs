/* eslint-disable no-await-in-loop */

/**
 *
 * @param {import("..").RedisClient} rc
 * @param {String} key
 */
export const lock = async (rc, x) => {
  while (!(await rc.sadd('lock', x))) {
    await new Promise((res) => setTimeout(res, 50));
  }
};

export const unlock = async (rc, x) => {
  await rc.srem('lock', x);
};
