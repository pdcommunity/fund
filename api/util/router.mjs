/* eslint-disable immutable/no-mutation */

export const router = async (params, ar) => {
  const x = ar.find(({ key }) => params.ctx.state.parsedUrl.slice(0, key.length) === key);
  if (x === undefined) {
    // eslint-disable-next-line no-param-reassign
    params.ctx.status = 404;
    // eslint-disable-next-line no-param-reassign
    params.ctx.body = {
      ok: false,
      reason: 'not found',
    };
    return;
  }
  // eslint-disable-next-line no-param-reassign
  params.ctx.state.parsedUrl = params.ctx.state.parsedUrl.slice(x.key.length);
  await x.callback(params);
};
