const subscriptions = new WeakMap();
/**
 * @type {Map<String, WebSocket[]>}
 */
const subscriptors = new Map();
const getMySub = (ws) => {
  if (!subscriptions.has(ws)) {
    subscriptions.set(ws, []);
  }
  return subscriptions.get(ws);
};

const getNameSub = (ws) => {
  if (!subscriptors.has(ws)) {
    subscriptors.set(ws, []);
  }
  return subscriptors.get(ws);
};

export const emit = (name, value) => {
  getNameSub(name).forEach(element => {
    element.send(JSON.stringify({
      type: 'subscribe-event',
      name,
      value,
    }));
  });
};

const removeMySub = (a, b) => {
  subscriptions.set(a, getMySub(a).filter(x => x !== b));
};

const removeNameSub = (a, b) => {
  subscriptors.set(a, getNameSub(a).filter(x => x !== b));
};

export const websocketHandler = async (ctx) => {
  ctx.websocket.on('message', async (message) => {
    const ws = ctx.websocket;
    const x = JSON.parse(message);
    if (x.type === 'subscribe') {
      getMySub(ws).push(x.name);
      getNameSub(x.name).push(ws);
    }
    if (x.type === 'unsubscribe') {
      removeMySub(ws, x.name);
      removeNameSub(x.name, ws);
    }
  });
  ctx.websocket.on('close', () => {
    console.log('closed');
  });
};
