export interface RedisClient {
  incrby: (key: String, value: Number) => Promise<void>,
  get: (key: String) => any,
};