export const getProjectObj = async (id, rc) => {
  const d = await rc.get(`project:${id}`);
  if (!d) throw new Error('not found');
  const x = JSON.parse(d);
  // that is ok and have better performance
  // eslint-disable-next-line immutable/no-mutation
  x.moneyHave = await rc.get(`money:${id}`);
  return x;
};
