/* eslint-disable immutable/no-mutation */

import { router } from "../util/router.mjs";
import { transferMoney } from "../money/transfer.mjs";
import { getProjectObj } from "./projectObj.mjs";

const listHandler = async ({ ctx, rc }) => {
  const ids = await rc.smembers('project');
  ctx.body = {
    ok: true,
    value: await Promise.all(ids.map((id) => getProjectObj(id, rc))),
  };
};

const getHandler = async ({ ctx, rc }) => {
  const id = ctx.state.parsedUrl;
  try {
    ctx.body = {
      ok: true,
      ...await getProjectObj(id, rc),
    };
  } catch (e) {
    ctx.body = {
      ok: false,
      reason: e.message,
    };
  }
};

const payHandler = async ({ ctx, rc }) => {
  const { token, projectId, price } = ctx.request.body;
  const username = await rc.get(`token:${token}`);
  if (!username) {
    ctx.body = {
      ok: false, reason: 'auth failed',
    };
    return;
  }
  try {
    console.log('salam');
    await transferMoney(rc, username, projectId, price);
    console.log('alte');
    ctx.body = {
      ok: true,
    };
  } catch (e) {
    console.log('salam2');
    ctx.body = {
      ok: false,
      reason: e.message,
    };
  }
};

export const createProject = async (rc, body) => {
  const b = body;
  await rc.set(`project:${b.id}`, JSON.stringify(b));
  await rc.set(`money:${b.id}`, 0);
  await rc.sadd('project', b.id);
};

export const createHandler = async ({ ctx, rc }) => {
  const { body } = ctx.request;
  if (!await rc.sadd('names', body.id)) {
    ctx.body = { ok: false, reason: 'duplicate username' };
    return;
  }
  await createProject(rc, body);
  ctx.body = { ok: true };
};

export const projectHandler = (params) => {
  return router(params, [
    { key: '/list', callback: listHandler },
    { key: '/get/', callback: getHandler },
    { key: '/new', callback: listHandler },
    { key: '/pay', callback: payHandler },
  ]);
};
